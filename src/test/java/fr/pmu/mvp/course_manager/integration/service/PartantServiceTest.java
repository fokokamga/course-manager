package fr.pmu.mvp.course_manager.integration.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.domain.Partant;
import fr.pmu.mvp.course_manager.model.PartantDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import fr.pmu.mvp.course_manager.repos.PartantRepository;
import fr.pmu.mvp.course_manager.service.PartantServiceImpl;
import fr.pmu.mvp.course_manager.util.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PartantServiceTest {

    @Mock
    private PartantRepository mockPartantRepository;
    @Mock
    private CourseRepository mockCourseRepository;

    private PartantServiceImpl partantServiceUnderTest;

    @BeforeEach
    void setUp() {
        partantServiceUnderTest = new PartantServiceImpl(mockPartantRepository, mockCourseRepository);
    }

    @Test
    void testFindAll() {
        // Setup
        // Configure PartantRepository.findAll(...).
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant.setName("name");
        partant.setNum(0);
        final Course course = new Course();
        course.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course.setPartants(Set.of(new Partant()));
        partant.setCourse(course);
        final List<Partant> partants = List.of(partant);
        when(mockPartantRepository.findAll(Sort.by("num"))).thenReturn(partants);

        // Run the test
        final List<PartantDTO> result = partantServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindAll_PartantRepositoryReturnsNoItems() {
        // Setup
        when(mockPartantRepository.findAll(Sort.by("num"))).thenReturn(Collections.emptyList());

        // Run the test
        final List<PartantDTO> result = partantServiceUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGet() {
        // Setup
        // Configure PartantRepository.findById(...).
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant1.setName("name");
        partant1.setNum(0);
        final Course course = new Course();
        course.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course.setPartants(Set.of(new Partant()));
        partant1.setCourse(course);
        final Optional<Partant> partant = Optional.of(partant1);
        when(mockPartantRepository.findById(UUID.fromString("957054fb-9817-458b-83f4-8a2f7381a9c2")))
                .thenReturn(partant);

        // Run the test
        final PartantDTO result = partantServiceUnderTest.get(UUID.fromString("957054fb-9817-458b-83f4-8a2f7381a9c2"));

        // Verify the results
    }

    @Test
    void testGet_PartantRepositoryReturnsAbsent() {
        // Setup
        when(mockPartantRepository.findById(UUID.fromString("957054fb-9817-458b-83f4-8a2f7381a9c2")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.get(
                UUID.fromString("957054fb-9817-458b-83f4-8a2f7381a9c2"))).isInstanceOf(NotFoundException.class);
    }

    @Test
    void testCreate() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        // Configure CourseRepository.findById(...).
        final Course course1 = new Course();
        course1.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course1.setName("name");
        course1.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant.setName("name");
        partant.setNum(0);
        course1.setPartants(Set.of(partant));
        final Optional<Course> course = Optional.of(course1);
        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"))).thenReturn(course);

        // Configure PartantRepository.save(...).
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant1.setName("name");
        partant1.setNum(0);
        final Course course2 = new Course();
        course2.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course2.setName("name");
        course2.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course2.setPartants(Set.of(new Partant()));
        partant1.setCourse(course2);
        when(mockPartantRepository.save(any(Partant.class))).thenReturn(partant1);

        // Run the test
        final PartantDTO result = partantServiceUnderTest.create(partantDTO);

        // Verify the results
    }

    @Test
    void testCreate_CourseRepositoryReturnsAbsent() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.create(partantDTO)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void testCreate_PartantRepositoryThrowsOptimisticLockingFailureException() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        // Configure CourseRepository.findById(...).
        final Course course1 = new Course();
        course1.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course1.setName("name");
        course1.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant.setName("name");
        partant.setNum(0);
        course1.setPartants(Set.of(partant));
        final Optional<Course> course = Optional.of(course1);
        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"))).thenReturn(course);

        when(mockPartantRepository.save(any(Partant.class))).thenThrow(OptimisticLockingFailureException.class);

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.create(partantDTO))
                .isInstanceOf(OptimisticLockingFailureException.class);
    }

    @Test
    void testUpdate() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        // Configure PartantRepository.findById(...).
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant1.setName("name");
        partant1.setNum(0);
        final Course course = new Course();
        course.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course.setPartants(Set.of(new Partant()));
        partant1.setCourse(course);
        final Optional<Partant> partant = Optional.of(partant1);
        when(mockPartantRepository.findById(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed")))
                .thenReturn(partant);

        // Configure CourseRepository.findById(...).
        final Course course2 = new Course();
        course2.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course2.setName("name");
        course2.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant2 = new Partant();
        partant2.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant2.setName("name");
        partant2.setNum(0);
        course2.setPartants(Set.of(partant2));
        final Optional<Course> course1 = Optional.of(course2);
        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5")))
                .thenReturn(course1);

        // Configure PartantRepository.save(...).
        final Partant partant3 = new Partant();
        partant3.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant3.setName("name");
        partant3.setNum(0);
        final Course course3 = new Course();
        course3.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course3.setName("name");
        course3.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course3.setPartants(Set.of(new Partant()));
        partant3.setCourse(course3);
        when(mockPartantRepository.save(any(Partant.class))).thenReturn(partant3);

        // Run the test
        partantServiceUnderTest.update(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed"), partantDTO);

        // Verify the results
        verify(mockPartantRepository).save(any(Partant.class));
    }

    @Test
    void testUpdate_PartantRepositoryFindByIdReturnsAbsent() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        when(mockPartantRepository.findById(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.update(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed"),
                partantDTO)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void testUpdate_CourseRepositoryReturnsAbsent() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        // Configure PartantRepository.findById(...).
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant1.setName("name");
        partant1.setNum(0);
        final Course course = new Course();
        course.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course.setPartants(Set.of(new Partant()));
        partant1.setCourse(course);
        final Optional<Partant> partant = Optional.of(partant1);
        when(mockPartantRepository.findById(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed")))
                .thenReturn(partant);

        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.update(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed"),
                partantDTO)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void testUpdate_PartantRepositorySaveThrowsOptimisticLockingFailureException() {
        // Setup
        final PartantDTO partantDTO = new PartantDTO();
        partantDTO.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partantDTO.setName("name");
        partantDTO.setNum(0);
        partantDTO.setCourse(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5"));

        // Configure PartantRepository.findById(...).
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant1.setName("name");
        partant1.setNum(0);
        final Course course = new Course();
        course.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        course.setPartants(Set.of(new Partant()));
        partant1.setCourse(course);
        final Optional<Partant> partant = Optional.of(partant1);
        when(mockPartantRepository.findById(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed")))
                .thenReturn(partant);

        // Configure CourseRepository.findById(...).
        final Course course2 = new Course();
        course2.setId(UUID.fromString("ed55b360-b797-4e22-afb6-6f263e271e40"));
        course2.setName("name");
        course2.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant2 = new Partant();
        partant2.setId(UUID.fromString("d8aab371-cf80-4576-ac73-7446ceb9deb5"));
        partant2.setName("name");
        partant2.setNum(0);
        course2.setPartants(Set.of(partant2));
        final Optional<Course> course1 = Optional.of(course2);
        when(mockCourseRepository.findById(UUID.fromString("05a94b9c-76fc-4185-9d85-37f2b71940a5")))
                .thenReturn(course1);

        when(mockPartantRepository.save(any(Partant.class))).thenThrow(OptimisticLockingFailureException.class);

        // Run the test
        assertThatThrownBy(() -> partantServiceUnderTest.update(UUID.fromString("57d5d25b-01b2-40bd-8f76-a5d4192cc6ed"),
                partantDTO)).isInstanceOf(OptimisticLockingFailureException.class);
    }

    @Test
    void testDelete() {
        // Setup
        // Run the test
        partantServiceUnderTest.delete(UUID.fromString("e4388835-108f-4c50-a300-a0202f596e16"));

        // Verify the results
        verify(mockPartantRepository).deleteById(UUID.fromString("e4388835-108f-4c50-a300-a0202f596e16"));
    }

    @Test
    void testNumExists() {
        // Setup
        when(mockPartantRepository.existsByNum(0)).thenReturn(false);

        // Run the test
        final boolean result = partantServiceUnderTest.numExists(0);

        // Verify the results
        assertThat(result).isFalse();
    }
}
