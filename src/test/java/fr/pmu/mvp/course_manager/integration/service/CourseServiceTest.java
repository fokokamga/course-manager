package fr.pmu.mvp.course_manager.integration.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.domain.Partant;
import fr.pmu.mvp.course_manager.model.CourseDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import fr.pmu.mvp.course_manager.service.CourseService;
import fr.pmu.mvp.course_manager.service.CourseServiceImpl;
import fr.pmu.mvp.course_manager.util.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.OptimisticLockingFailureException;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseServiceTest {

    @Mock
    private CourseRepository mockCourseRepository;

    private CourseServiceImpl courseServiceUnderTest;

    @BeforeEach
    void setUp() {
        courseServiceUnderTest = new CourseServiceImpl(mockCourseRepository);
    }

    @Test
    void testFindAll() {
        // Setup
        // Configure CourseRepository.findAll(...).
        final Course course = new Course();
        course.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant.setName("name");
        partant.setNum(0);
        course.setPartants(Set.of(partant));
        final List<Course> courses = List.of(course);
        when(mockCourseRepository.findAll()).thenReturn(courses);

        // Run the test
        final List<CourseDTO> result = courseServiceUnderTest.findAll();

        // Verify the results
    }

    @Test
    void testFindAll_CourseRepositoryReturnsNoItems() {
        // Setup
        when(mockCourseRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<CourseDTO> result = courseServiceUnderTest.findAll();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGet() {
        // Setup
        // Configure CourseRepository.findById(...).
        final Course course1 = new Course();
        course1.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course1.setName("name");
        course1.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant.setName("name");
        partant.setNum(0);
        course1.setPartants(Set.of(partant));
        final Optional<Course> course = Optional.of(course1);
        when(mockCourseRepository.findById(UUID.fromString("48494b6e-e29b-476d-9a02-43483c1bbf4d"))).thenReturn(course);

        // Run the test
        final CourseDTO result = courseServiceUnderTest.get(UUID.fromString("48494b6e-e29b-476d-9a02-43483c1bbf4d"));

        // Verify the results
    }

    @Test
    void testGet_CourseRepositoryReturnsAbsent() {
        // Setup
        when(mockCourseRepository.findById(UUID.fromString("48494b6e-e29b-476d-9a02-43483c1bbf4d")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(
                () -> courseServiceUnderTest.get(UUID.fromString("48494b6e-e29b-476d-9a02-43483c1bbf4d")))
                .isInstanceOf(NotFoundException.class);
    }

    @Test
    void testCreate() {
        // Setup
        final CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        courseDTO.setName("name");
        courseDTO.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));

        // Configure CourseRepository.save(...).
        final Course course = new Course();
        course.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course.setName("name");
        course.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant.setName("name");
        partant.setNum(0);
        course.setPartants(Set.of(partant));
        when(mockCourseRepository.save(any(Course.class))).thenReturn(course);

        // Run the test
        final CourseDTO result = courseServiceUnderTest.create(courseDTO);

        // Verify the results
    }

    @Test
    void testCreate_CourseRepositoryThrowsOptimisticLockingFailureException() {
        // Setup
        final CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        courseDTO.setName("name");
        courseDTO.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));

        when(mockCourseRepository.save(any(Course.class))).thenThrow(OptimisticLockingFailureException.class);

        // Run the test
        assertThatThrownBy(() -> courseServiceUnderTest.create(courseDTO))
                .isInstanceOf(OptimisticLockingFailureException.class);
    }

    @Test
    void testUpdate() {
        // Setup
        final CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        courseDTO.setName("name");
        courseDTO.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));

        // Configure CourseRepository.findById(...).
        final Course course1 = new Course();
        course1.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course1.setName("name");
        course1.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant.setName("name");
        partant.setNum(0);
        course1.setPartants(Set.of(partant));
        final Optional<Course> course = Optional.of(course1);
        when(mockCourseRepository.findById(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40"))).thenReturn(course);

        // Configure CourseRepository.save(...).
        final Course course2 = new Course();
        course2.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course2.setName("name");
        course2.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant1 = new Partant();
        partant1.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant1.setName("name");
        partant1.setNum(0);
        course2.setPartants(Set.of(partant1));
        when(mockCourseRepository.save(any(Course.class))).thenReturn(course2);

        // Run the test
        courseServiceUnderTest.update(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40"), courseDTO);

        // Verify the results
        verify(mockCourseRepository).save(any(Course.class));
    }

    @Test
    void testUpdate_CourseRepositoryFindByIdReturnsAbsent() {
        // Setup
        final CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        courseDTO.setName("name");
        courseDTO.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));

        when(mockCourseRepository.findById(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40")))
                .thenReturn(Optional.empty());

        // Run the test
        assertThatThrownBy(() -> courseServiceUnderTest.update(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40"),
                courseDTO)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void testUpdate_CourseRepositorySaveThrowsOptimisticLockingFailureException() {
        // Setup
        final CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        courseDTO.setName("name");
        courseDTO.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));

        // Configure CourseRepository.findById(...).
        final Course course1 = new Course();
        course1.setId(UUID.fromString("144c5801-df4d-4bfe-aeb0-1c4ab09bdbba"));
        course1.setName("name");
        course1.setDate(LocalDateTime.of(2020, 1, 1, 0, 0, 0));
        final Partant partant = new Partant();
        partant.setId(UUID.fromString("de32b9b6-10ec-45ef-90e2-91769cdd1166"));
        partant.setName("name");
        partant.setNum(0);
        course1.setPartants(Set.of(partant));
        final Optional<Course> course = Optional.of(course1);
        when(mockCourseRepository.findById(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40"))).thenReturn(course);

        when(mockCourseRepository.save(any(Course.class))).thenThrow(OptimisticLockingFailureException.class);

        // Run the test
        assertThatThrownBy(() -> courseServiceUnderTest.update(UUID.fromString("927640ad-bdac-485e-bdd9-a686bb179d40"),
                courseDTO)).isInstanceOf(OptimisticLockingFailureException.class);
    }

    @Test
    void testDelete() {
        // Setup
        // Run the test
        courseServiceUnderTest.delete(UUID.fromString("b02dce04-88d0-4aaf-bec0-51636083d7de"));

        // Verify the results
        verify(mockCourseRepository).deleteById(UUID.fromString("b02dce04-88d0-4aaf-bec0-51636083d7de"));
    }
}
