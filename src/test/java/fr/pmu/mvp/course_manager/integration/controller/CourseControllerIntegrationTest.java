package fr.pmu.mvp.course_manager.integration.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.model.CourseDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class CourseControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CourseRepository courseRepository;

    private CourseDTO courseDTO1;
    private CourseDTO courseDTO2;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        courseRepository.deleteAll();

        courseDTO1 = new CourseDTO();
        courseDTO1.setId(UUID.randomUUID());
        courseDTO1.setName("course1");
        courseDTO1.setDate(LocalDateTime.of(2023, 3, 8,12,0,4));

        courseDTO2 = new CourseDTO();
        courseDTO2.setId(UUID.randomUUID());
        courseDTO2.setName("course2");
        courseDTO2.setDate(LocalDateTime.of(2023, 3, 9,12,10,9));

    }

    @Test
    void given2Courses_whenMockMVC_thenReturns2() throws Exception {
        courseRepository.save(mapToEntity(courseDTO1, new Course()));
        courseRepository.save(mapToEntity(courseDTO2, new Course()));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/courses")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        objectMapper.findAndRegisterModules();
        List<CourseDTO> courses = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<>() {
                });

        assertEquals(2, courses.size());
    }



    @Test
    void givenCourseIdURI_whenMockMVC_thenVerifyResponse200() throws Exception {
        Course course = courseRepository.save(mapToEntity(courseDTO1, new Course()));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/courses/{id}", course.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(200, result.getResponse().getStatus());
        objectMapper.findAndRegisterModules();
        CourseDTO courseDTO = objectMapper.readValue(result.getResponse().getContentAsString(), CourseDTO.class);

        assertEquals(courseDTO1.getName(), courseDTO.getName());
        assertEquals(courseDTO1.getDate(), courseDTO.getDate());
    }

    @Test
    void testGetWithInvalidId() throws Exception {
        UUID invalidId = UUID.randomUUID();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/courses/{id}", invalidId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }

    @Test
    void givenCoursesURI_whenMockMVC_thenVerifyCourseIsCreateAndResponse201() throws Exception {
        objectMapper.findAndRegisterModules();

        String requestJson = objectMapper.writeValueAsString(courseDTO1);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/courses")
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(201, result.getResponse().getStatus());
        CourseDTO courseDTO = objectMapper.readValue(result.getResponse().getContentAsString(), CourseDTO.class);

        assertEquals(courseDTO1.getName(), courseDTO.getName());
        assertEquals(courseDTO1.getDate(), courseDTO.getDate());

        List<Course> courses = courseRepository.findAll();
        assertEquals(1, courses.size());
    }

    private Course mapToEntity(final CourseDTO courseDTO, final Course course) {
        course.setName(courseDTO.getName());
        course.setDate(courseDTO.getDate());
        return course;
    }
}


