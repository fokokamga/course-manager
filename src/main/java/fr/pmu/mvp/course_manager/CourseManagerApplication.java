package fr.pmu.mvp.course_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CourseManagerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(CourseManagerApplication.class, args);
    }

}
