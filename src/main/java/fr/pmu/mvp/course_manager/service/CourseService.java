package fr.pmu.mvp.course_manager.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.model.CourseDTO;

import java.util.List;
import java.util.UUID;

public interface CourseService {
    List<CourseDTO> findAll();

    CourseDTO get(UUID id);

    CourseDTO create(CourseDTO courseDTO);

    void update(UUID id, CourseDTO courseDTO);

    void delete(UUID id);

    default CourseDTO mapToDTO(Course course, CourseDTO courseDTO) {
        courseDTO.setId(course.getId());
        courseDTO.setName(course.getName());
        courseDTO.setDate(course.getDate());
        return courseDTO;
    }

    default Course mapToEntity(CourseDTO courseDTO, Course course) {
        course.setName(courseDTO.getName());
        course.setDate(courseDTO.getDate());
        return course;
    }
}
