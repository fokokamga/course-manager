package fr.pmu.mvp.course_manager.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.domain.Partant;
import fr.pmu.mvp.course_manager.model.PartantDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import fr.pmu.mvp.course_manager.repos.PartantRepository;
import fr.pmu.mvp.course_manager.util.NotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class PartantServiceImpl implements PartantService {

    private final PartantRepository partantRepository;
    private final CourseRepository courseRepository;

    public PartantServiceImpl(final PartantRepository partantRepository,
                              final CourseRepository courseRepository) {
        this.partantRepository = partantRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<PartantDTO> findAll() {
        final List<Partant> partants = partantRepository.findAll(Sort.by("num"));
        return partants.stream()
                .map((partant) -> mapToDTO(partant, new PartantDTO()))
                .collect(Collectors.toList());
    }

    @Override
    public PartantDTO get(final UUID id) {
        return partantRepository.findById(id)
                .map(partant -> mapToDTO(partant, new PartantDTO()))
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public PartantDTO create(final PartantDTO partantDTO) {
        final Partant partant = new Partant();
        mapToEntity(partantDTO, partant);
        return mapToDTO(partantRepository.save(partant), new PartantDTO());
    }

    @Override
    public void update(final UUID id, final PartantDTO partantDTO) {
        final Partant partant = partantRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(partantDTO, partant);
        partantRepository.save(partant);
    }

    @Override
    public void delete(final UUID id) {
        partantRepository.deleteById(id);
    }
    @Override
    public PartantDTO mapToDTO(final Partant partant, final PartantDTO partantDTO) {
        partantDTO.setId(partant.getId());
        partantDTO.setName(partant.getName());
        partantDTO.setNum(partant.getNum());
        partantDTO.setCourse(partant.getCourse() == null ? null : partant.getCourse().getId());
        return partantDTO;
    }
    @Override
    public Partant mapToEntity(final PartantDTO partantDTO, final Partant partant) {
        partant.setName(partantDTO.getName());
        partant.setNum(partantDTO.getNum());
        final Course course = partantDTO.getCourse() == null ? null : courseRepository.findById(partantDTO.getCourse())
                .orElseThrow(() -> new NotFoundException("course not found"));
        partant.setCourse(course);
        return partant;
    }

    @Override
    public boolean numExists(final Integer num) {
        return partantRepository.existsByNum(num);
    }

}
