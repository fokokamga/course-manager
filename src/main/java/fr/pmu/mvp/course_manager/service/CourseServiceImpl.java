package fr.pmu.mvp.course_manager.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.model.CourseDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import fr.pmu.mvp.course_manager.util.NotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;


@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    public CourseServiceImpl(final CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<CourseDTO> findAll() {
        final List<Course> courses = courseRepository.findAll();
        return courses.stream()
                .map((course) -> mapToDTO(course, new CourseDTO()))
                .collect(Collectors.toList());
    }

    @Override
    public CourseDTO get(final UUID id) {
        return courseRepository.findById(id)
                .map(course -> mapToDTO(course, new CourseDTO()))
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public CourseDTO create(final CourseDTO courseDTO) {
        final Course course = new Course();
        mapToEntity(courseDTO, course);
        return mapToDTO(courseRepository.save(course),new CourseDTO());
    }

    @Override
    public void update(final UUID id, final CourseDTO courseDTO) {
        final Course course = courseRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(courseDTO, course);
        courseRepository.save(course);
    }

    @Override
    public void delete(final UUID id) {
        courseRepository.deleteById(id);
    }

}
