package fr.pmu.mvp.course_manager.service;

import fr.pmu.mvp.course_manager.domain.Course;
import fr.pmu.mvp.course_manager.domain.Partant;
import fr.pmu.mvp.course_manager.model.PartantDTO;
import fr.pmu.mvp.course_manager.repos.CourseRepository;
import fr.pmu.mvp.course_manager.util.NotFoundException;

import java.util.List;
import java.util.UUID;

public interface PartantService {

    List<PartantDTO> findAll();

    PartantDTO get(UUID id);

    PartantDTO create(PartantDTO partantDTO);

    void update(UUID id, PartantDTO partantDTO);

    void delete(UUID id);

     PartantDTO mapToDTO(Partant partant, PartantDTO partantDTO) /*{
        partantDTO.setId(partant.getId());
        partantDTO.setName(partant.getName());
        partantDTO.setNum(partant.getNum());
        partantDTO.setCourse(partant.getCourse() == null ? null : partant.getCourse().getId());
        return partantDTO*/;
    //};

     Partant mapToEntity(PartantDTO partantDTO, Partant partant) /*{
        partant.setName(partantDTO.getName());
        partant.setNum(partantDTO.getNum());
        final Course course = partantDTO.getCourse() == null ? null : courseRepository.findById(partantDTO.getCourse())
                .orElseThrow(() -> new NotFoundException("course not found"));
        partant.setCourse(course);
        return partant*/;
        //}

    boolean numExists(Integer num);
}
