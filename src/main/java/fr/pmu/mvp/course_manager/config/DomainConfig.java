package fr.pmu.mvp.course_manager.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("fr.pmu.mvp.course_manager.domain")
@EnableJpaRepositories("fr.pmu.mvp.course_manager.repos")
@EnableTransactionManagement
public class DomainConfig {
}
