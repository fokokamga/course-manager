package fr.pmu.mvp.course_manager.repos;

import fr.pmu.mvp.course_manager.domain.Partant;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PartantRepository extends JpaRepository<Partant, UUID> {

    boolean existsByNum(Integer num);

}
