package fr.pmu.mvp.course_manager.repos;

import fr.pmu.mvp.course_manager.domain.Course;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CourseRepository extends JpaRepository<Course, UUID> {
}
