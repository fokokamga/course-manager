package fr.pmu.mvp.course_manager.model;

import lombok.Getter;
import lombok.Setter;
import jakarta.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class BaseModelDTO implements  BaseModel{
    private UUID id;

    @Size(max = 255)
    private String name;
}
