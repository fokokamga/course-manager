package fr.pmu.mvp.course_manager.model;

import jakarta.validation.constraints.Size;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PartantDTO extends BaseModelDTO{

    private Integer num;

    private UUID course;

}
