package fr.pmu.mvp.course_manager.model;

import jakarta.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CourseDTO extends BaseModelDTO{

    private LocalDateTime date;

}
