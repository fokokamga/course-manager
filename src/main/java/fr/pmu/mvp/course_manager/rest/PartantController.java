package fr.pmu.mvp.course_manager.rest;

import fr.pmu.mvp.course_manager.model.PartantDTO;
import fr.pmu.mvp.course_manager.service.PartantService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/partants", produces = MediaType.APPLICATION_JSON_VALUE)
public class PartantController {

    private final PartantService partantService;
    @Autowired
    KafkaTemplate<String, PartantDTO> partantKafkaTemplate;


    public PartantController(final PartantService partantService) {
        this.partantService = partantService;
    }

    @GetMapping
    public ResponseEntity<List<PartantDTO>> getAllPartants() {
        return ResponseEntity.ok(partantService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PartantDTO> getPartant(@PathVariable(name = "id") final UUID id) {
        return ResponseEntity.ok(partantService.get(id));
    }

    @PostMapping
    @ApiResponse(responseCode = "201")
    public ResponseEntity<PartantDTO> createPartant(@RequestBody @Valid final PartantDTO partantDTO) {
        // On envoie l'objet crée au topic kafka
        partantKafkaTemplate.send("partant_pmu",partantService.create(partantDTO));

        return new ResponseEntity<>(partantService.create(partantDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updatePartant(@PathVariable(name = "id") final UUID id,
            @RequestBody @Valid final PartantDTO partantDTO) {
        partantService.update(id, partantDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    public ResponseEntity<Void> deletePartant(@PathVariable(name = "id") final UUID id) {
        partantService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
