# CourseManager

This MVP racing management application is an API that allows you to create races and their participants, store the information in a database, and expose it to the rest of the system through a message published on a bus.

## Development

During development it is recommended to use the profile `local`. In IntelliJ, `-Dspring.profiles.active=local` can be added in the VM options of the Run Configuration after enabling this property in "Modify options".

This application use an embedded database (H2)

After starting the application it is accessible under `localhost:3000`.

The swagger documentation is accessible under `http://localhost:3000/swagger-ui/index.html`



## Build

The application can be built using the following command:

```
gradlew clean build
```

The application can then be started with the following command - here with the profile `production`:

```
java -Dspring.profiles.active=production -jar ./build/libs/course-manager-0.0.1-SNAPSHOT.jar
```

## Test the app with Kafka
You can see kafka configuration in application.properties.
You can easily start kafka with docker compose. You need to install Docker and Docker Compose locally.
Next enter the folder `kafka` with your terminal and execute this command to create and deploy the containers.

```
docker-compose -f kafka-zookeper-compose.yml up
```
After that, you can signIn to the Kafka UI to see the datas in the topics. It is accessible under `http://localhost:8080/admin/login` .

login: `admin@admin.io`
password: `admin`

- Conduktor-platform: `$DOCKER_HOST_IP:8080`
- Single Zookeeper: `$DOCKER_HOST_IP:2181`
- Single Kafka: `$DOCKER_HOST_IP:9092`
- Kafka Schema Registry: `$DOCKER_HOST_IP:8081`
- Kafka Rest Proxy: `$DOCKER_HOST_IP:8082`
- Kafka Connect: `$DOCKER_HOST_IP:8083`
- KSQL Server: `$DOCKER_HOST_IP:8088`
- (experimental) JMX port at `$DOCKER_HOST_IP:9001`


